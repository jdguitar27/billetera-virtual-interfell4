import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './services/axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/sass/styles.scss'




Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.prototype.$http = axios;

async function createApp() {
  await store.dispatch('auth/loadUserDataFromLocalStorage');
  // await store.dispatch('auth/verifyToken');

  return new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}

const app = createApp();
