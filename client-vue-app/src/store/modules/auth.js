import {use} from "element-ui/src/locale";
import router from '../../router';
import axios from '../../services/axios';
import { Message } from 'element-ui';

export default {
  namespaced: true,
  state: {
    user: null,
    token: ''
  },
  getters: {
    user: state => state.user,
    loggedIn: state => state.token !== '',
    token: state => state.token
  },
  mutations: {
    setUser: (state, user) => {
      state.user = user;
    },
    setToken: (state, token) => {
      state.token = token;
    },
  },
  actions: {
    /**
     * Almacena los datos del usuario en el estado global y en local storage.
     * @param userData Contiene los datos del usuario que ha iniciado sesión y su respectivo token
     */
    saveUser({dispatch}, userData) {
      dispatch('setUserAndToken', {
        user: userData.user,
        token: userData.accessToken
      });
      localStorage.setItem('userData', JSON.stringify(userData));
    },

    /**
     * Analiza el localstorage para extraer los datos del usuario que tenga una sesión abierta.
     * @param dispatch
     */
    loadUserDataFromLocalStorage({dispatch}) {
      const userData = JSON.parse(localStorage.getItem('userData'));
      if (userData) {
        dispatch('setUserAndToken', {
          user: userData.user,
          token: userData.accessToken
        });
        axios.defaults.headers['x-access-token'] = userData.accessToken;
      }
    },

    async verifyToken({dispatch, getters}) {
      if(getters.token && getters.token !== '') {
        try {
          const res = await axios.get('/api/auth/verifyToken');
          dispatch('setUserAndToken', {
            user: res.data,
            token: res.data.accessToken
          });
        } catch (e) {
          if (e.response.status == 401) {
            dispatch('logout');
            Message.error({
              message: 'Session expired',
              showClose: true
            });
          }
        }
      }
    },

    /**
     * Cierra la sesión del usuario eliminando su información del estado y del localstorage.
     */
    logout({dispatch}) {
      dispatch('setUserAndToken', {
        user: null,
        token: ''
      });
      localStorage.removeItem('userData');
      router.push({ name: 'login' });
    },

    /**
     * Muta el estado cambiando el valor del user y el token.
     */
    setUserAndToken({commit}, data) {
      commit('setToken', data.token);
      commit('setUser', data.user);
    }
  }
}