export default {
    methods: {
        /**
         * Muestra un mensaje de notificación
         * @param message {string} Texto que aparece en el mensaje.
         * @param type {string} Define el tipo de mensaje que se va a mostrar.
         */
        openMessage(message, type) {
            this.$message[type]({
                message: message,
                showClose: true
            });
        }
    }
};
