export default {
    methods: {
        /**
         * Extrae el mensaje del error. Evalua distintos tipos de error para extraer
         * el string correctamente.
         * @param error Error a evaluar para extraer su mensaje.
         * @return {string} Mensaje del error.
         */
        getErrorString(error) {
            console.log(error);
            let errorString = '';
            if (error.response.data.hasOwnProperty('errors')) {
                const errData = error.response.data;
                for (let err in errData.errors) {
                    errData.errors[err].map(message => {
                        errorString += message + '<br/>';
                    });
                }
            } else if(error.response.data.hasOwnProperty('message')) {
                errorString = error.response.data.message;
            } else {
                if(error.response.status == 401)
                    errorString = 'Sus credenciales son incorrectas. Usuario o contraseña inválidos';
                else
                    errorString = error.response.data;
            }

            return errorString;
        }
    }
}
