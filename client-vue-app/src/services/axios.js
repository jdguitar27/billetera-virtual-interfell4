import axios from 'axios';
import store from "../store";

const axiosInstance = axios.create({
  baseURL: 'http://localhost:8081/'
});

// axiosInstance.interceptors.response.use(function (response) {
//   return response;
// }, async function (error) {
//   // console.log(error)
//   if (error.response.status === 401) {
//     const data = localStorage.getItem('userData');
//     if(data && data.hasOwnProperty('accessToken')) {
//       store.dispatch('auth/logout');
//       alert('Su sesión ha expirado');
//     }
//   } else {
//     return Promise.reject(error);
//   }
// });

export default axiosInstance;