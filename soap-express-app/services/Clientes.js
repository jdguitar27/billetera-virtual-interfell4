const {User} = require('../models');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {Op} = require("sequelize");
const sequelize = require("sequelize");
const {throwSoapError, decodificarToken, enviarEmail} = require('../utils/utilsFunctions');

module.exports = {
  /**
   * Registra un cliente en la plataforma.
   * @param nombre {string} Nombre del cliente.
   * @param documento {string} Número de documento del cliente.
   * @param celular {string} Número del celular del cliente.
   * @param email {string} Email del cliente.
   * @param password {string} Contraseña para la cuenta.
   */
  async registroCliente({nombre, documento, celular, email, password}) {
    try {
      if (!nombre || nombre.length === 0)
        throw new Error('El nombre no es válido.');

      if (!documento || documento.length === 0)
        throw new Error('El documento no es válido.');

      if (!celular || celular.length === 0)
        throw new Error('El número de celular no es válido.');

      if (!email || email.length === 0)
        throw new Error('El email no es válido.');

      if (!password || password.length === 0)
        throw new Error('El password no es válido.');

      /**
       * Consultando si el cliente ya ha sido registrado.
       */
      let user = await User.findOne({
        where: {
          [Op.or]: [
            {email},
            {documento},
            {celular}
          ]
        }
      });

      if (user)
        throw new Error('El email, documento o celular ya ha sido registrado.');

      // Creación del cliente
      user = await User.create({
        nombre,
        documento,
        celular,
        email,
        password: bcrypt.hashSync(password, 8)
      });

      enviarEmail(user.email,  `Bienvenido a tu billetera virtual, ${user.nombre}`, 'Bienvenido');
      return {mensaje: "Cliente registrado correctamente"};
    } catch (err) {
      console.log(err);
      // return {message: "Error, no se ha podido registrar el cliente"};
      throwSoapError(err.message, 500);
    }
  },

  /**
   * Inicia la sesión de un cliente en la plataforma.
   * @param email {string} Email del usuario para iniciar sesión.
   * @param password {string} Contraseña del ususario para iniciar sesión.
   */
  async accesoCliente({email, password}) {
    try {
      if (!email || email.length === 0)
        throw new Error('El email no es válido.');

      if (!password || password.length === 0)
        throw new Error('El password no es válido.');

      /**
       * Consultando si el cliente ya ha sido registrado.
       */
      let user = await User.findOne({
        where: {
          email
        }
      });

      if (!user)
        throw new Error('El usuario no se encuentra registrado.');

      /**
       * Comparación de la contraseña.
       */
      const passwordIsValid = bcrypt.compareSync(
        password,
        user.password
      );

      if (!passwordIsValid)
        throw new Error('Contraseña inválida');

      const token = jwt.sign({userId: user.id}, process.env.JWT_SECRET, {
        expiresIn: 86400 // 24 hours
      });

      user.password = null;
      return {
        mensaje: 'Ha iniciado sesión correctamente',
        user: user.dataValues,
        accessToken: token
      };
    } catch (err) {
      console.log(err);
      throwSoapError(err.message, 500);
    }
  },

  /**
   * Recarga saldo a la billetera del cliente.
   * @param documento {string} Número de documento de identificación del cliente.
   * @param celular {string} Número de celular del cliente.
   * @param valor {number} Valor a agregar al saldo de la billetera.
   */
  async recargarBilletera({documento, celular, valor}, cb, headers) {
    try {
      const usuario = await decodificarToken(headers);

      if(!usuario)
        throw new Error('Token inválido o no se encuentra autenticado');

      if (!documento || documento.length === 0)
        throw new Error('El documento no es válido.');

      if (!celular || celular.length === 0)
        throw new Error('El número de celular no es válido.');

      /**
       * Consultando si el cliente ya ha sido registrado.
       */
      let user = await User.findOne({
        where: {
          id: usuario.id,
          documento,
          celular
        }
      });

      if (!user)
        throw new Error('La combinación del documento y el número de celular no coinciden con el usuario autenticado.');

      valor = parseFloat(valor);
      if (isNaN(valor))
        throw new Error('El valor a agregar en la billetera es inválido.');

      if (valor < 0)
        throw new Error('El valor debe ser un monto positivo.');

      user.saldo += valor;
      await user.save();

      return {
        mensaje: 'Recarga de billetera realizada correctamente'
      };
    } catch (err) {
      console.log(err);
      throwSoapError(err.message, 500);
    }
  },

  /**
   * Consulta el saldo en la billetera del cliente.
   * @param documento {string} Número de documento de identificación del cliente.
   * @param celular {string} Número de celular del cliente.
   */
  async consultarSaldo({documento, celular}, cb, headers) {
    try {
      console.log(headers);
      const usuario = await decodificarToken(headers);

      if(!usuario)
        throw new Error('Token inválido o no se encuentra autenticado');

      if (!documento || documento.length === 0)
        throw new Error('El documento no es válido.');

      if (!celular || celular.length === 0)
        throw new Error('El número de celular no es válido.');

      /**
       * Consultando si el cliente ya ha sido registrado.
       */
      let user = await User.findOne({
        where: {
          id: usuario.id,
          documento,
          celular
        }
      });

      if (!user)
        throw new Error('La combinación del documento y el número de celular no coinciden con el usuario autenticado.');

      return {
        saldo: user.saldo
      };
    } catch (err) {
      console.log(err);
      throwSoapError(err.message,  500);
    }
  },
};