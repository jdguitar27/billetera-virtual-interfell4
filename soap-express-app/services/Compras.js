const {User, Compra} = require('../models');
const {throwSoapError, generateRandomString, enviarEmail, decodificarToken} = require('../utils/utilsFunctions');

module.exports = {
  /**
   * Registra una compra del usuario en el sistema.
   * @param descripcion {string} Descripcion de la compra a realizar.
   * @param monto {number} Monto de la compra.
   */
  async realizarCompra({descripcion, monto}, cb, headers) {
    try {
      const user = await decodificarToken(headers);

      if (!user)
        throw new Error('Token inválido o no se encuentra autenticado');

      if (!descripcion || descripcion.length === 0)
        throw new Error('La descripcion no es válida.');

      monto = parseFloat(monto);
      if (isNaN(monto))
        throw new Error('El monto de la compra es inválido.');

      if (monto < 0)
        throw new Error('El monto de la compra debe ser positivo.');

      const randomString = generateRandomString(6);
      // Creación del cliente
      const compra = await Compra.create({
        descripcion,
        monto,
        usuario: user.id,
        token: randomString
      });

      enviarEmail(user.email, `Identificador del pago: ${compra.id}
              Código de verificación de la compra: ${randomString}`, `Código de compra Nº${compra.id}`);

      return {mensaje: "Compra registrada correctamente. Se ha enviado un código de verificación a su correo."};
    } catch (err) {
      console.log(err);
      throwSoapError(err.message, 500);
    }
  },

  /**
   * Permite confirmar la resta de saldo de una compra realizada previamente.
   * @param idPago {number} Identificador de la compra del usuario.
   * @param token {string} Token de validación enviado al correo del usuario.
   */
  async confirmarPago({idPago, token}, cb, headers) {
    try {
      const user = await decodificarToken(headers);

      if (!user)
        throw new Error('Token inválido o no se encuentra autenticado');

      if (!idPago)
        throw new Error('EL identificador del pago no es válido.');

      if (!token || token.length != 6 )
        throw new Error('EL token no es válido.');

      /**
       * Datos del usuario.
       */
      const compra = await Compra.findOne({
        where: {
          id: idPago,
          usuario: user.id,
          token,
          realizada: null
        }
      });

      if (!compra)
        throw new Error('No se encontraron registros para el pago que intenta realizar');

      if(user.saldo < compra.monto)
        throw new Error('No posee saldo suficiente para procesar el pago.');

      user.saldo -= compra.monto;
      compra.realizada = new Date();
      await user.save();
      await compra.save();

      return {mensaje: "Pago procesado exitosamente."};
    } catch (err) {
      console.log(err);
      throwSoapError(err.message, 500);
    }
  },
};