const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const soap = require('soap');
const app = express();
require('dotenv').config();

const corsOptions = {
  origin: process.env.REST_API_ORIGIN_URL
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

/**
 * ----------------------------------------
 * Listado de servicios para el servidor.
 * ----------------------------------------
 */
const Clientes = require('./services/Clientes');
const Compras = require('./services/Compras');

const services = {
  BilleteraVirtual: {
    Billetera: {
      ...Clientes,
      ...Compras
    }
  }
};

const xml = require('fs').readFileSync('BilleteraVirtual.wsdl', 'utf8');

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
  console.log(`http://localhost:${PORT}.`);

  soap.listen(app, '/wsdl', services, xml, function(){
    console.log('\nSOAP server initialized');
  });
});
