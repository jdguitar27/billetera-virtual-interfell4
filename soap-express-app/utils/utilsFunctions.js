const {User} = require('../models');
const emailSender = require('../config/mail');
const jwt = require("jsonwebtoken");

module.exports = {
  /**
   * Arroja un error SOAP para las respuestas de las peticiones de los servicios.
   * @param text { string } Mensaje del error
   * @param code { number|string } Código del error
   */
  throwSoapError(text, code = 500) {
    throw {
      Fault: {
        Code: {
          Value: code
        },
        Reason: {Text: text}
      }
    };
  },

  /**
   * Envía un email al correo proporcionado.
   * @param emailTo {string} Email al cual se va a enviar el correo.
   * @param text {string} Contenido del mensaje.
   * @param subject {string} Asunto del email a enviar.
   * @param html {boolean} Especifica si el contenido de text es html o un string simple.
   * Por defecto está inicializado en false.
   */
  enviarEmail(emailTo, text, subject, html = false) {
    const mailOptions = {
      from: 'jdcodetest@gmail.com',
      to: emailTo,
      subject: subject,
    };

    if (html)
      mailOptions.html = text;
    else
      mailOptions.text = text;

    emailSender.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  },

  /**
   * Genera un string random según la longitud que se le pase por parametro.
   * @param length {number} Longitud del string a generar.
   * @returns {string} String random generado.
   */
  generateRandomString(length) {
    length = length < 1 ? 6 : length;
    return Math.random().toString(36).substr(2, length);
  },

  /**
   * Decodifica el token pasara como parametro
   * @param headers {Object} Headers de la peticion.
   * @return {null|User} Retorna el usuario autenticado si el token el válido, sino retorna nulo
   */
  async decodificarToken(headers) {
    let tokenResult = null;
    let token = headers && headers.hasOwnProperty('token') ? headers.token : null;
    if (token) {
      await jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
        if (!err) {
          tokenResult = await User.findByPk(decoded.userId);
        }
      });
    }
    return tokenResult;
  }
}