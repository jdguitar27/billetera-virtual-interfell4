# Documentación de Billetera Virtual
El proyecto ha sido realizado utilizando NodeJs, ExpressJs Y MySQL para los servicios SOAP 
y REST. Solamente el servidor SOAP tiene acceso a la base de datos. 
Para manejar la conexión a la base de datos se utiliza el ORM Sequelize.
En el Front End se utiliza el framework VueJS y las peticiones se realizan utilizando Axios.
La autenticación de los usuarios es una basada en tokens utilizando JWT.

### Instalación
Al clonar por primera vez el repositorio del proyecto se deben ejecutar los siguientes comandos
en el mismo orden en que se encuentran:

1. `cd soap-express-app && npm install`
2. `cd rest-express-app && npm install`
3. `cd client-vue-app && npm install`

Luego renombrar los archivos .env.example en cada uno de los sistemas a .env

### Inicialización de base de datos
Crear una base de datos en MySQL y colocar sus datos de conexión en los archivos
**/soap-express-app/config/config.json** y **/soap-express-app/.env**

Y ejecutar las migraciones con el siguiente comando dentro del directorio 
**/soap-express-app**:
`sequelize db:migrate`

Este comando creará las tablas necesarias en la base de datos configurada en el
paso anterior.

### Orden de ejecución de las aplicaciones.
El orden ideal para ejecutar los servicios y el front son los siguientesÑ
1. `cd soap-express-app && npm run serve`
2. `cd rest-express-app && npm run serve`
3. `cd client-vue-app && npm run serve`
